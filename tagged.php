<?php

/**
 * Plugin Name:       Tagged
 * Description:       Tagged Recaptcha Plugin.
 * Version:           1.0.4
 * Author:            CwebConsultants
 * Author URI:        http://www.cwebconsultants.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Tagged
 */
// If this file is called directly, abort.
if (!defined('WPINC'))
    die;

define('ADV_PLUGIN_FS_PATH1', plugin_dir_path(__FILE__));
define('ADV_PLUGIN_WS_PATH1', plugin_dir_url(__FILE__));

//Declares Common Fucntion File 
require plugin_dir_path(__FILE__) . 'includes/function/function.php';

//admin part
{
    require plugin_dir_path(__FILE__) . 'includes/classes/class-tagged-admin.php';
    $plugin_admin = new Tagged_Admin('tagged', '1.0.2');
    add_action('admin_enqueue_scripts', array($plugin_admin, 'enqueue_styles'));
    add_action('admin_enqueue_scripts', array($plugin_admin, 'enqueue_scripts'));
}

add_filter('comment_form_submit_button', 'tg_show_recaptcha_in_comments', 10, 2);
add_filter('preprocess_comment', 'tg_check_comment', 0);
add_action('register_form', 'tg_show_recaptcha_in_register');
add_filter('registration_errors', 'tg_check_register');
register_activation_hook(__FILE__, 'tg_plugin_activate');
?>