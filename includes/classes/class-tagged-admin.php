<?php
/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class Tagged_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @var      string    $plugin_name       The name of this plugin.
     * @var      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        add_action('admin_menu', array(&$this, 'register_my_custom_menu_page'));

        // Hook into the 'init' action Staff
        add_action('init', array(&$this, 'create_tagged_posttype'));

        // Hook into the 'init' action Current Menu
        add_action('admin_head', array(&$this, 'cwebc_active_current_menu'));

        //Hide default Riders Post Type Menu Item from Admin Menu                
        add_action('admin_menu', array(&$this, 'cwebc_remove_weather_menu_items'));

        /* Add Custom Column for codes post */
//        add_filter('manage_codes_posts_columns', 'wpse188743_codes_cpt_columns');

        function wpse188743_codes_cpt_columns($columns) {
            unset($columns['date']);
            $new_columns = array(
                'cityname' => __('City', 'weather'),
                'sortcode' => __('Shortcode', 'weather'),
                'date' => __('Date', 'weather')
            );

            return array_merge($columns, $new_columns);
        }

//        add_action('manage_codes_posts_custom_column', 'my_manage_codes_columns', 10, 2);

        function my_manage_codes_columns($column, $post_id) {

            global $citiesArray;
            switch ($column) {
                case 'cityname' :
                    $city_name = get_post_meta($post_id, 'Location', true);
                    if (!empty($city_name)):
                        echo $citiesArray[$city_name]['name'];
                    endif;
                    break;
                case 'sortcode' :
                    $city_code = get_post_meta($post_id, 'citySortCode', true);
                    if (!empty($city_code)):
                        echo "[weather id='" . $city_code . "']";
                    endif;
                    break;
            }
        }

    }

    /**
     * Register the stylesheets for the Dashboard.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(dirname(dirname(__FILE__))) . 'admin/css/weather-admin.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . '-icon', plugin_dir_url(dirname(dirname(__FILE__))) . 'public/css/weather-icons.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the dashboard.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
    }

    function register_my_custom_menu_page() {
        global $submenu;
        add_menu_page(__('Weather', 'weather'), __('TAGS CAPTCHA', 'weather'), 'read', 'settings', array(&$this, 'manage_settings'), plugins_url('tagged/admin/img/refresh_icon.gif'));
        add_submenu_page('settings', __('Image Tags', 'weather'), __('Image Tags', 'weather'), 'read', 'tagged', array(&$this, 'manage_tagged'));
        add_submenu_page('settings', __('Settings', 'weather'), __('Settings', 'weather'), 'read', 'settings', array(&$this, 'manage_settings'));

        //$submenu['daily-diary'][0][0] = 'Daily Diary';
    }

    /*
     * Staff Fucntion
     */

    function manage_tagged() {
        if (!current_user_can('read')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'weather'));
        } else {
            echo '<div align="center"><div class="spinner-myimg"></div></div>';
            $url = admin_url() . 'edit.php?post_type=tagged';
            ?>
            <script>location.href = '<?php echo $url; ?>';</script>
            <?php
        }
    }

    /*
     * Setting Fucntion
     */

    function manage_settings() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'weather'));
        } else {
            include(ADV_PLUGIN_FS_PATH1 . 'includes/classes/classSettings.php');
        }
    }

    /*
     * Hide Menus
     */

    function cwebc_remove_weather_menu_items() {
        remove_menu_page('edit.php?post_type=tagged');
    }

    // Register Custom Post Type
    function create_tagged_posttype() {
        $labels = array(
            'name' => _x('Tagged Images', 'Post Type General Name', 'tagged'),
            'singular_name' => _x('Tagged Images', 'Post Type Singular Name', 'tagged'),
            'menu_name' => __('Tagged Images', 'tagged'),
            'all_items' => __('All Tagged Images', 'tagged'),
            'view_item' => __('View Tagged Images', 'tagged'),
            'add_new_item' => __('Add New Tagged Image', 'tagged'),
            'add_new' => __('Add New Tagged Image', 'tagged'),
            'edit_item' => __('Edit Tagged Images', 'tagged'),
            'update_item' => __('Update Tagged Images', 'tagged'),
            'search_items' => __('Search Tagged Images', 'tagged'),
            'not_found' => __('Not found', 'tagged'),
            'not_found_in_trash' => __('Not found in Trash', 'tagged'),
        );
        $args = array(
            'labels' => $labels,
            'description' => __('Description.', 'Tagged'),
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'tagged'),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array('title', 'thumbnail')
        );
        register_post_type('tagged', $args);
    }

    function cwebc_active_current_menu() {
        //Code for show current menu
        $screen = get_current_screen();

        if ($screen->id == 'edit-tagged' || $screen->id == 'tagged') {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    jQuery('#toplevel_page_settings').addClass('wp-menu-open');
                    jQuery('#toplevel_page_settings').addClass('wp-has-current-submenu');
                    jQuery('#toplevel_page_settings>a').addClass('wp-has-current-submenu');

                    jQuery('a[href$="codes"]').parent().addClass('current');
                    jQuery('.toplevel_page_weather').addClass('current');
                    jQuery('.toplevel_page_weather').parent().addClass('wp-has-current-submenu');

                });
            </script>
            <?php
        }
    }

}
