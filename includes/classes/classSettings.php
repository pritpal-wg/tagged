<?php
$number_of_image = get_option('number_of_image');
$number_of_question = get_option('number_of_question');

/* Update Settings */
if (isset($_POST['submit_changes'])) {
    update_option('number_of_image', $_POST['number_of_image']);
    update_option('number_of_question', $_POST['number_of_question']);
    set_error_message(_e('CAPTCHA Image Settings has been updated succesfully', 'Tagged'), '0');
    foreceRedirect(admin_url('admin.php?page=settings'));
    exit;
}
?>

<div class="wrap">

    <form method="POST">
        <div id="header-area"> 
            <div class="logo">
                <!--<div class="emg-icon-option-left"></div>-->
                <div class="emg-cp-title"><h2><?php echo __('TAGS CAPTCHA SECTION', 'Tagged'); ?></h2></div>
            </div>
        </div>
        <div id="main-area">
            <?php show_error_message(); ?>

            <!--Start Bootstrap -->
            <div class="row">
                <div class="_main-row">


                    <div class="col-xs-9 right-op-panel padding0">
                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="tab-pane" id="media">
                                <div class="of-info-cy-main">
                                    <h3 style="margin: 0"><?php echo __('TAGS CAPTCHA Settings'); ?></h3>
                                </div>
                                <table style="width: 100%" class="setting_table">
                                    <tr></tr>  

                                    <tr>
                                        <td style='width:220px;'>
                                            <label><?php echo __('Number of images to use in CAPTCHA', 'weather'); ?></label>
                                        </td>
                                        <td style='width:400px;'>
                                            <input type="number" min="1" name="number_of_image" id="captcha_image" value="<?php echo $number_of_image; ?>" />

                                        </td>

                                    </tr>

                                    <tr><td>&nbsp;</td></tr>

                                    <tr>
                                        <td style='width:250px;'><label><?php echo __('Number of questions to show in CAPTCHA'); ?></label></td>
                                        <td style='width:400px;'>
                                            <input type='number' min="1" name='number_of_question' id="captcha_question" value="<?php echo $number_of_question; ?>"/>
                                        </td>
                                    </tr>							
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <input type="submit" name="submit_changes" value="Save Settings" class="button-primary" style="float: right;margin-top: 2%"  />
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </form>
</div>
