<?php
/* Function for session message */

function set_error_message($msg, $type) {
    @session_start();
    if (isset($_SESSION['error_msg'])):
        unset($_SESSION['error_msg']);
    endif;
    $_SESSION['error_msg']['msg'] = $msg;
    $_SESSION['error_msg']['error'] = $type;
    return true;
}

function show_error_message() {
    $msg = '';
    @session_start();
    if (isset($_SESSION['error_msg']) && isset($_SESSION['error_msg']['msg'])):
        if ($_SESSION['error_msg']['error'] == '1'):
            $tp = 'weather_error';
        else:
            $tp = 'weather_success';
        endif;
        $msg.='<div class="message center ' . $tp . '">';
        $msg.=$_SESSION['error_msg']['msg'];
        $msg.='</div>';
        unset($_SESSION['error_msg']['msg']);
        unset($_SESSION['error_msg']['error']);
        unset($_SESSION['error_msg']);
    endif;

    echo $msg;
}

function foreceRedirect($filename) {
    if (!headers_sent())
        header('Location: ' . $filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . $filename . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
        echo '</noscript>';
    }
}

if (!function_exists('pr')) {

    function pr($e) {
        echo "<pre>";
        print_r($e);
        echo "</pre>";
    }

}

if (!function_exists('vd')) {

    function vd($e) {
        echo "<pre>";
        var_dump($e);
        echo "</pre>";
    }

}

function tg_InvalidCaptcha($tg_cpt) {
    $error = false;
    return $error;
}

function tg_show_recaptcha_in_register() {
    //get the number of images and the questions
    {
        $number_of_image = intval(get_option('number_of_image'));
        if (!$number_of_image)
            return;
        $number_of_question = intval(get_option('number_of_question'));
        if (!$number_of_image)
            return;
    }

    //get unique random tags
    {
        global $wpdb;
        $sql = "SELECT DISTINCT meta_value as tag FROM `{$wpdb->postmeta}` WHERE meta_key='tag' AND post_id IN (SELECT DISTINCT ID FROM `{$wpdb->posts}` WHERE post_type='tagged' AND {$wpdb->posts}.post_status = 'publish') GROUP BY post_id ORDER BY RAND() LIMIT $number_of_question";
        $data = $wpdb->get_results($sql);
        if (empty($data))
            return;
        $tags = array();
        foreach ($data as $e)
            $tags[] = $e->tag;
    }

    //get tags used for questions
    $question_tags = $tags;
    //get images for tags
    {
        $args = array(
            'post_type' => 'tagged',
            'posts_per_page' => -1,
            'orderby' => 'rand',
            'fields' => 'ids',
            'meta_query' => array(
                array(
                    'key' => 'tag',
                    'value' => $tags,
                    'compare' => 'IN'
                ),
            ),
        );
        $query = new WP_Query($args);
        $tagged_posts = $query->posts;
        wp_reset_query();
        if (count($tagged_posts) != $number_of_image) {
            $final_tagged_posts = array();
            if (count($tagged_posts) < $number_of_image) {
                //images received are not sufficient
                $remaining_images = $number_of_image - count($tagged_posts);
                $final_tagged_posts = $tagged_posts;
                $args = array(
                    'post_type' => 'tagged',
                    'posts_per_page' => $remaining_images,
                    'orderby' => 'rand',
                    'fields' => 'ids',
                    'meta_query' => array(
                        array(
                            'key' => 'tag',
                            'value' => $tags,
                            'compare' => 'NOT IN'
                        ),
                    ),
                );
                $query = new WP_Query($args);
                $new_tagged_posts = $query->posts;
                wp_reset_query();
                $final_tagged_posts = array_merge($final_tagged_posts, $new_tagged_posts);
            } else {
                //extra images received
                $tags_used = array();
                foreach ($tagged_posts as $tagged_id) {
                    $post_tag = get_post_meta($tagged_id, 'tag', true);
                    if (!in_array($post_tag, $tags_used)) {
                        $tags_used[] = $post_tag;
                        $final_tagged_posts[] = $tagged_id;
                    }
                }
            }
            $tagged_posts = $final_tagged_posts;
        }
        shuffle($tagged_posts);
        $images = array();
        foreach ($tagged_posts as $tagged_id) {
            $img_data = wp_get_attachment_image_src(get_post_thumbnail_id($tagged_id), 'medium');
            $images[] = $img_data[0];
        }
    }
    ob_start();
    ?>
    <style>
        body #login {
            width: 350px;
        }

        .tg_captcha_container {
            /*margin: 0 auto;*/
            padding: 4px;
            background: #5B9BD5;
            width: 100%;
            margin-bottom: 10px;
        }

        .tg_captcha_container .captcha_canvas {
            width: 100%;
            border: solid 1px green;
            box-sizing: border-box;
            margin-right: 2%;
            background: #fff;
            margin-bottom: 5px;
            padding: 5px 0 0 5px;
        }

        .tg_captcha_container .captcha_questions {
            /*float: left;*/
            /*width: 35%;*/
            border: solid 2px grey;
            box-sizing: border-box;
            padding: 1%;
            background: #fff;
            height: 140px;
            overflow: auto;
            box-sizing: border-box;
        }

        .tg_captcha_container .captcha_questions ul {
            list-style-type: decimal;
            padding-left: 30px;
        }

        .tg_captcha_container .captcha_questions ul li:not(:last-child) {
            margin-bottom: 5px;
        }

        .clearfix {
            box-sizing: border-box;
            clear: both;
        }
    </style>
    <?php ?>
    <script>
        window.onload = function () {
            function gcd(a, b) {
                if (!b)
                    return a;
                return gcd(b, a % b);
            }
            var c = document.getElementById("tg_canvas");
            var ctx = c.getContext("2d");
            var images = JSON.parse('<?php echo json_encode($images) ?>');
            //get image size
            {
                var x = c.width;
                var y = c.height;
                var n = images.length;
                var px = Math.ceil(Math.sqrt(n * x / y));
                var py = Math.ceil(Math.sqrt(n * y / x));
                var sx, sy;
                if (Math.floor(px * y / x) * px < n)
                    sx = y / Math.ceil(px * y / x);
                else
                    sx = x / px;
                if (Math.floor(py * x / y) * py < n)
                    sy = x / Math.ceil(x * py / y);
                else
                    sy = y / py;
                var num_in_x = x / sx;
                var num_in_y = y / sy;
            }
            var percent_over = 10;
            var _img;
            var xx = 0;
            var yy = 0;
            var i = 0;
            var imgArray = [];
            for (var j = 0; j < num_in_y; j++) {
                imgArray[j] = [];
                for (var k = 0; k < num_in_x; k++) {
                    _img = images[i];
                    if (_img) {
                        (function (j, k, xx, yy, sx, sy, url) {
                            imgArray[j][k] = new Image();
                            imgArray[j][k].src = url;
                            imgArray[j][k].onload = function () {
                                percent_over = Math.abs(percent_over);
                                if (percent_over > 100)
                                    percent_over = 100;
                                var new_yy = yy - ((percent_over / 100) * yy);
                                var new_xx = xx - ((percent_over / 100) * xx);
                                ctx.drawImage(imgArray[j][k], new_xx, new_yy, sx, sy);
                            };
                        })(j, k, xx, yy, sx, sy, _img);
                        i++;
                        xx += sx;
                    }
                }
                xx = 0;
                yy += sy;
            }
        };
    </script>
    <div id="tg_captcha_container" class="tg_captcha_container">
        <div class="captcha_canvas">
            <canvas id="tg_canvas"></canvas>
        </div>
        <div class="captcha_questions">
            <div style="margin-bottom: 10px;">Please write down the number of:</div>
            <input type="hidden" name="tg_cpt[tag_post_ids]" value="<?php echo implode(',', $tagged_posts) ?>" />
            <ul>
                <?php foreach ($question_tags as $tag) { ?>
                    <li><?php echo ucfirst($tag) ?> <input type="number" min="1" name="tg_cpt[tags][<?php echo $tag ?>]" style="width: 50px;padding: 0 0 0 5px" /></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php
    $captcha = ob_get_clean();
    echo '<label for="verification">Verification:</label>';
    echo $captcha;
//    return $captcha . $submit_button;
}

function tg_check_register($data) {
    if (!isset($_POST['tg_cpt'])) {
        $data->add('blank_captcha', '<b>ERROR:</b> Invalid Recaptcha Response');
        return $data;
    }
    $tg_cpt = $_POST['tg_cpt'];
    unset($_POST['tg_cpt']);
    $tg_post_ids = explode(',', $tg_cpt['tag_post_ids']);
    if (empty($tg_post_ids)) {
        $data->add('blank_captcha', '<b>ERROR:</b> Invalid Recaptcha Response');
        return $data;
    }
    if (!is_array($tg_cpt['tags'])) {
        $data->add('blank_captcha', '<b>ERROR:</b> Invalid Recaptcha Response');
        return $data;
    }
    $error = false;
    foreach ($tg_cpt['tags'] as $given_answer)
        if (!$given_answer || !is_numeric($given_answer) || intval($given_answer) <= 0)
            $error = true;
    if ($error) {
        $data->add('blank_captcha', '<b>ERROR:</b> Invalid Recaptcha Response');
        return $data;
    }
    $tg_tags = array_keys($tg_cpt['tags']);
    if (empty($tg_tags)) {
        $data->add('blank_captcha', '<b>ERROR:</b> Invalid Recaptcha Response');
        return $data;
    }
    $correct_answers = array();
    foreach ($tg_post_ids as $k => $post_id) {
        $post_tag = get_post_meta($post_id, 'tag', true);
        if (!in_array($post_tag, $tg_tags)) {
            unset($tg_post_ids[$k]);
        } else {
            $num = intval(get_post_meta($post_id, 'number', true));
            if (array_key_exists($post_tag, $correct_answers))
                $correct_answers[$post_tag] += $num;
            else
                $correct_answers[$post_tag] = $num;
        }
    }
    foreach ($tg_cpt['tags'] as $_tag => $given_answer) {
        if ($given_answer != $correct_answers[$_tag])
            $error = true;
    }
    if ($error) {
        $data->add('blank_captcha', '<b>ERROR:</b> Invalid Recaptcha Response');
    }
    return $data;
}

function tg_show_recaptcha_in_comments($submit_button, $args) {
    global $user_ID;
    //get the number of images and the questions
    {
        $number_of_image = intval(get_option('number_of_image'));
        if (!$number_of_image)
            return $submit_button;
        $number_of_question = intval(get_option('number_of_question'));
        if (!$number_of_image)
            return $submit_button;
    }

    //get unique random tags
    {
        global $wpdb;
        $sql = "SELECT DISTINCT meta_value as tag FROM `{$wpdb->postmeta}` WHERE meta_key='tag' AND post_id IN (SELECT DISTINCT ID FROM `{$wpdb->posts}` WHERE post_type='tagged' AND {$wpdb->posts}.post_status = 'publish') GROUP BY post_id ORDER BY RAND() LIMIT $number_of_question";
        $data = $wpdb->get_results($sql);
        if (empty($data))
            return $submit_button;
        $tags = array();
        foreach ($data as $e)
            $tags[] = $e->tag;
    }

    //get tags used for questions
    $question_tags = $tags;
    //get images for tags
    {
        $args = array(
            'post_type' => 'tagged',
            'posts_per_page' => -1,
            'orderby' => 'rand',
            'fields' => 'ids',
            'meta_query' => array(
                array(
                    'key' => 'tag',
                    'value' => $tags,
                    'compare' => 'IN'
                ),
            ),
        );
        $query = new WP_Query($args);
        $tagged_posts = $query->posts;
        wp_reset_query();
        if (count($tagged_posts) != $number_of_image) {
            $final_tagged_posts = array();
            if (count($tagged_posts) < $number_of_image) {
                //images received are not sufficient
                $remaining_images = $number_of_image - count($tagged_posts);
                $final_tagged_posts = $tagged_posts;
                $args = array(
                    'post_type' => 'tagged',
                    'posts_per_page' => $remaining_images,
                    'orderby' => 'rand',
                    'fields' => 'ids',
                    'meta_query' => array(
                        array(
                            'key' => 'tag',
                            'value' => $tags,
                            'compare' => 'NOT IN'
                        ),
                    ),
                );
                $query = new WP_Query($args);
                $new_tagged_posts = $query->posts;
                wp_reset_query();
                $final_tagged_posts = array_merge($final_tagged_posts, $new_tagged_posts);
            } else {
                //extra images received
                $tags_used = array();
                foreach ($tagged_posts as $tagged_id) {
                    $post_tag = get_post_meta($tagged_id, 'tag', true);
                    if (!in_array($post_tag, $tags_used)) {
                        $tags_used[] = $post_tag;
                        $final_tagged_posts[] = $tagged_id;
                    }
                }
            }
            $tagged_posts = $final_tagged_posts;
        }
        shuffle($tagged_posts);
        $images = array();
        foreach ($tagged_posts as $tagged_id) {
            $img_data = wp_get_attachment_image_src(get_post_thumbnail_id($tagged_id), 'medium');
            $images[] = $img_data[0];
        }
    }
    ob_start();
    ?>
    <style>
        .tg_captcha_container {
            margin: 0 auto;
            padding: 4px;
            background: #5B9BD5;
            width: 510px;
            margin-bottom: 10px;
            text-align: center;
        }

        .tg_captcha_container .captcha_canvas {
            display: inline-block;
            width: 312px;
            border: solid 1px green;
            box-sizing: border-box;
            background: #fff;
            padding: 5px 0 0 5px;
        }

        .tg_captcha_container .captcha_questions {
            display: inline-block;
            width: 185px;
            border: solid 1px grey;
            box-sizing: border-box;
            padding: 5px;
            background: #fff;
            text-align: left;
            font-size: 14px;
        }

        .tg_captcha_container .captcha_questions ul {
            list-style-type: decimal;
            padding: 0;
            margin: 0;
            padding-left: 30px;
        }

        .tg_captcha_container .captcha_questions ul li input[type="number"] {
            font-size: 13px;
        }
        .tg_captcha_container .captcha_questions ul li:not(:last-child) {
            margin-bottom: 3px;
        }

        .clearfix {
            box-sizing: border-box;
            clear: both;
        }
    </style>
    <?php ?>
    <script>
        window.onload = function () {
            var inputs = document.querySelectorAll('.captcha_questions input[type="number"]');
            for (var i in inputs) {
                var input = inputs[i];
                input.value = null;
            }
            function gcd(a, b) {
                if (!b)
                    return a;
                return gcd(b, a % b);
            }
            var c = document.getElementById("tg_canvas");
            var ctx = c.getContext("2d");
            var images = JSON.parse('<?php echo json_encode($images) ?>');
            //get image size
            {
                var x = c.width;
                var y = c.height;
                var n = images.length;
                var px = Math.ceil(Math.sqrt(n * x / y));
                var py = Math.ceil(Math.sqrt(n * y / x));
                var sx, sy;
                if (Math.floor(px * y / x) * px < n)
                    sx = y / Math.ceil(px * y / x);
                else
                    sx = x / px;
                if (Math.floor(py * x / y) * py < n)
                    sy = x / Math.ceil(x * py / y);
                else
                    sy = y / py;
                var num_in_x = x / sx;
                var num_in_y = y / sy;
            }
            var percent_over = 10;
            var _img;
            var xx = 0;
            var yy = 0;
            var i = 0;
            var imgArray = [];
            for (var j = 0; j < num_in_y; j++) {
                imgArray[j] = [];
                for (var k = 0; k < num_in_x; k++) {
                    _img = images[i];
                    if (_img) {
                        (function (j, k, xx, yy, sx, sy, url) {
                            imgArray[j][k] = new Image();
                            imgArray[j][k].src = url;
                            imgArray[j][k].onload = function () {
                                percent_over = Math.abs(percent_over);
                                if (percent_over > 100)
                                    percent_over = 100;
                                var new_yy = yy - ((percent_over / 100) * yy);
                                var new_xx = xx - ((percent_over / 100) * xx);
                                ctx.drawImage(imgArray[j][k], new_xx, new_yy, sx, sy);
                            };
                        })(j, k, xx, yy, sx, sy, _img);
                        i++;
                        xx += sx;
                    }
                }
                xx = 0;
                yy += sy;
            }
        };
    </script>
    <div id="tg_captcha_container" class="tg_captcha_container">
        <div class="captcha_canvas">
            <canvas id="tg_canvas"></canvas>
        </div>
        <div class="captcha_questions">
            <div style="margin-bottom: 10px;">Please write down the number of:</div>
            <input type="hidden" name="tg_cpt[tag_post_ids]" value="<?php echo implode(',', $tagged_posts) ?>" />
            <ul>
                <?php foreach ($question_tags as $tag) { ?>
                    <li><?php echo ucfirst($tag) ?> <input type="number" min="1" name="tg_cpt[tags][<?php echo $tag ?>]" style="width: 50px;padding: 0 0 0 5px" /></li>
                    <?php } ?>
            </ul>
        </div>
    </div>
    <?php
    $captcha = ob_get_clean();
    return $captcha . $submit_button;
}

function tg_check_comment($data) {
    if ($data['comment_type'])
        return $data;
    if (!isset($_POST['tg_cpt']))
        return $data;
    $tg_cpt = $_POST['tg_cpt'];
    if (tg_InvalidCaptcha($tg_cpt)) {
        
    }
    unset($_POST['tg_cpt']);
    $tg_post_ids = explode(',', $tg_cpt['tag_post_ids']);
    if (empty($tg_post_ids)) {
        add_filter('pre_comment_approved', create_function('$a', 'return \'spam\';'));
        return $data;
    }
    if (!is_array($tg_cpt['tags'])) {
        add_filter('pre_comment_approved', create_function('$a', 'return \'spam\';'));
        return $data;
    }
    $error = false;
    foreach ($tg_cpt['tags'] as $given_answer)
        if (!$given_answer || !is_numeric($given_answer) || intval($given_answer) <= 0)
            $error = true;
    if ($error) {
        add_filter('pre_comment_approved', create_function('$a', 'return \'spam\';'));
        return $data;
    }
    $tg_tags = array_keys($tg_cpt['tags']);
    if (empty($tg_tags)) {
        add_filter('pre_comment_approved', create_function('$a', 'return \'spam\';'));
        return $data;
    }
    $correct_answers = array();
    foreach ($tg_post_ids as $k => $post_id) {
        $post_tag = get_post_meta($post_id, 'tag', true);
        if (!in_array($post_tag, $tg_tags)) {
            unset($tg_post_ids[$k]);
        } else {
            $num = intval(get_post_meta($post_id, 'number', true));
            if (array_key_exists($post_tag, $correct_answers))
                $correct_answers[$post_tag] += $num;
            else
                $correct_answers[$post_tag] = $num;
        }
    }
    foreach ($tg_cpt['tags'] as $_tag => $given_answer) {
        if ($given_answer != $correct_answers[$_tag])
            $error = true;
    }
    if ($error) {
        add_filter('pre_comment_approved', create_function('$a', 'return \'spam\';'));
    }
    return $data;
}

function tg_plugin_activate() {
    $dir = dirname(dirname(__DIR__)) . '/activation';
    $contents = scandir($dir);
    $wp_upload_dir = wp_upload_dir();
    // These files need to be included as dependencies when on the front end.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );
    foreach ($contents as $upload_image) {
        if (is_file($dir . "/$upload_image")) {
            $_ct = substr_count($upload_image, '_');
            if ($_ct >= 2) {
                $image_data = tg_ImageData($upload_image);
                $postarr = array(
                    'post_title' => $image_data['name'],
                    'post_type' => 'tagged',
                    'post_status' => 'publish',
                );
                $parent_post_id = wp_insert_post($postarr);
                if (!is_numeric($parent_post_id))
                    continue;
                update_post_meta($parent_post_id, 'tag', $image_data['tag']);
                update_post_meta($parent_post_id, 'number', $image_data['number']);
                $file_path = $dir . "/$upload_image";
                $filename = $wp_upload_dir['path'] . "/$upload_image";
                if (!is_file($filename))
                    copy($file_path, $filename);
                $filetype = wp_check_filetype(basename($filename), null);
                $attachment = array(
                    'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
                    'post_mime_type' => $filetype['type'],
                    'post_title' => $image_data['name'],
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                $attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);
                set_post_thumbnail($parent_post_id, $attach_id);
            }
        }
    }
    if (!get_option('number_of_question'))
        update_option('number_of_question', 4);
    if (!get_option('number_of_image'))
        update_option('number_of_image', 8);
}

function tg_ImageData($image) {
    $data = array();
    $ext = end(explode('.', $image));
    $data['ext'] = $ext;
    if (strpos($image, '.') !== FALSE)
        $image = substr($image, 0, strpos($image, "." . $ext));
    $img_data = explode('_', $image);
    $data['name'] = $img_data[0];
    $data['tag'] = $img_data[1];
    $data['number'] = $img_data[2];
    return $data;
}
?>