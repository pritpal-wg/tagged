 $(function() { 
	function displayResult(item) {
		selectWeatherCity(item.city);
	}
	
	function fullWeatherLoading(t) {
		jQuery(t).html("<div class='loader_data'></div>")
	}
	
	function get_api_data(t) { 
		$("#weather-header").css("margin-top","0px");
		$("#search").css("opacity","0.0");
		$("#weather-display").css("opacity","1.0");
		$("#weather-display").show();
		$("#weather-display").removeClass('hide');
		$("#weather-header").removeClass('hide');
		fullWeatherLoading("#weather-display");
		//return false;
		var e = location.protocol + "//" + document.domain + "/" + location.pathname.split("/")[1] + "/wp-content/plugins/weather/public/ajax.php";
		jQuery.ajax({
			type: "POST",
			url: e,
			data: "city=" + t + "&type=api",
			success: function(t) {
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$("#weather-header").css("margin-top","0px");
				$("#hide-weather").show();
				$("#weather-display").removeClass('hide');
				$("#weather-header").removeClass('hide');
				$("#weather-display").html(t)
				$(".searchCity").val('');
			}
		})
	}
	
	function selectWeatherCity(t) {
		if(t=='Result not Found'){
			$(".searchCity").val('');
			return false;
		}
		$("#weatherCityName").html(t), $("#weatherCityHead").show(), get_api_data(t)
	}
		
	function loadAutoComplete() { 
		var t = location.protocol + "//" + document.domain + "/" + location.pathname.split("/")[1] + "/wp-content/plugins/weather/public/typeahead.php";
		jQuery.ajax({
			type: "POST",
			url: t,
			data: "",
			success: function(response) { 
				$('.searchCity').typeahead({
                    source:response,
					displayField: 'name',
                    valueField: 'id',
                    onSelect: displayResult,
					minLength: 2,
					resultLimit: 5,
                });
			}
		})
	}

	loadAutoComplete();	

 });	