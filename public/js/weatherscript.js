// http://stackoverflow.com/questions/12259701/how-do-i-prevent-a-script-from-running-on-mobile-devices
function isMobile() {
return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

if (!isMobile()) {
//place script you don't want to run on mobile here
    // Tooltip
    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });
    // End Tooltip
}

// Close weather display
function hidemydiv() { 
	//document.getElementById('hide-weather').style.display = "none";
    //document.getElementById('search').style.opacity = "1.0";
	 $("#weather-header").css("margin-top","-100px");
     $("#search").css("opacity","1.0");
	 $("#weather-display").hide();
	 $("#weather-display").css("opacity","0");	
}


//Detailed prognosis horizontal scroller
$(function() {
			$(".table-responsive").mousewheel(function(event, delta) {
				this.scrollLeft -= (delta * 30);
				event.preventDefault();
			});
		});
		
 // Hide weather on end scroll script:
// stackoverflow.com/questions/25813508/how-to-hide-element-when-scroll-reaches-the-bottom-of-the-page
$(document).ready(function(){
var top=0;
$(window).scroll(function(){

    
if($(window).scrollTop() + window.innerHeight == $(document).height()){
 $("#hide-weather").css("opacity","0.0");
    $("#weather-header").css("margin-top","-100px");
     $("#search").css("opacity","1.0");
	 //$("#weather-display").hide();
	 $("#weather-display").css("opacity","0");
        
 }else{
 $("#hide-weather").css("opacity","1.0");
    $("#weather-header").css("margin-top","0px");
     $("#search").css("opacity","0.0");
	 $("#weather-display").css("opacity","1.0");
     //$("#weather-display").show();  
 }
});
});